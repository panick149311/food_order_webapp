
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/05/2017 17:20:25
-- Generated from EDMX file: C:\Users\PaNick\Documents\Visual Studio 2017\Projects\Food_Order_webapp\Food_Order_webapp\Models\Food_Order_model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Food_order_DB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'CustomerSet'
CREATE TABLE [dbo].[CustomerSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Fname] nvarchar(max)  NOT NULL,
    [Lname] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [address] nvarchar(max)  NOT NULL,
    [Phone_num] nvarchar(max)  NOT NULL,
    [Username] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'FoodSet'
CREATE TABLE [dbo].[FoodSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Image] nvarchar(max)  NOT NULL,
    [Price] nvarchar(max)  NOT NULL,
    [Detail] nvarchar(max)  NOT NULL,
    [CategoryId] int  NOT NULL
);
GO

-- Creating table 'OrderSet'
CREATE TABLE [dbo].[OrderSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Status] nvarchar(max)  NOT NULL,
    [CustomerId] int  NOT NULL,
    [Quantity] nvarchar(max)  NOT NULL,
    [Total_price] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CategorySet'
CREATE TABLE [dbo].[CategorySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'OrderFood'
CREATE TABLE [dbo].[OrderFood] (
    [Orders_Id] int  NOT NULL,
    [Foods_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'CustomerSet'
ALTER TABLE [dbo].[CustomerSet]
ADD CONSTRAINT [PK_CustomerSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FoodSet'
ALTER TABLE [dbo].[FoodSet]
ADD CONSTRAINT [PK_FoodSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrderSet'
ALTER TABLE [dbo].[OrderSet]
ADD CONSTRAINT [PK_OrderSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CategorySet'
ALTER TABLE [dbo].[CategorySet]
ADD CONSTRAINT [PK_CategorySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Orders_Id], [Foods_Id] in table 'OrderFood'
ALTER TABLE [dbo].[OrderFood]
ADD CONSTRAINT [PK_OrderFood]
    PRIMARY KEY CLUSTERED ([Orders_Id], [Foods_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [CustomerId] in table 'OrderSet'
ALTER TABLE [dbo].[OrderSet]
ADD CONSTRAINT [FK_CustomerOrder]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[CustomerSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerOrder'
CREATE INDEX [IX_FK_CustomerOrder]
ON [dbo].[OrderSet]
    ([CustomerId]);
GO

-- Creating foreign key on [CategoryId] in table 'FoodSet'
ALTER TABLE [dbo].[FoodSet]
ADD CONSTRAINT [FK_FoodCategory]
    FOREIGN KEY ([CategoryId])
    REFERENCES [dbo].[CategorySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FoodCategory'
CREATE INDEX [IX_FK_FoodCategory]
ON [dbo].[FoodSet]
    ([CategoryId]);
GO

-- Creating foreign key on [Orders_Id] in table 'OrderFood'
ALTER TABLE [dbo].[OrderFood]
ADD CONSTRAINT [FK_OrderFood_Order]
    FOREIGN KEY ([Orders_Id])
    REFERENCES [dbo].[OrderSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Foods_Id] in table 'OrderFood'
ALTER TABLE [dbo].[OrderFood]
ADD CONSTRAINT [FK_OrderFood_Food]
    FOREIGN KEY ([Foods_Id])
    REFERENCES [dbo].[FoodSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderFood_Food'
CREATE INDEX [IX_FK_OrderFood_Food]
ON [dbo].[OrderFood]
    ([Foods_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------